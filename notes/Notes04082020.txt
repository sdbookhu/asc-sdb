Notes from class 4/8/2020

Opening up the Rmarkdown document in Rcpp from last time. 

Best way to talk about these Rcpp pointers

cppFunction("void setzero(NumericVector v) {
	v[0] = 0;
}

Copy made, version inside Cpp was a copied and converted one, 

When made numeric and then passed it in, doesn't need to be coerced to another type.

NumericVector object is a pointer, can redefine types to have stars in them

1. NumericVector is a pointer, doesn't want to copy, if give argument of another type

Running accelerate on 408 machines, just type in Rmkl

Showing a file from the lectures directory

sat_laGP_cv_snow.R

This example does a big cross validation, fold.do

This function allows one fold of something, a regression between X's and Y's

nth <- number of threads

On arc we want to distinguish between node parallelize and within node parallelization

Shared memory parallelization environment, on one computer for within core -> good for openmp

But for between cores, we need to send multiple times between the computing nodes

Same potential to do 2 things at once, potentially computationally expensive

Justin will show how to do with mpi, message passing interface

fold.do is called, nth argument will create cross validation folds

Apply the fold.do function for all the folds

Have to do extra from the clusterApply to put them together. 

Principled with saving information, makes sense to see if it works without parallel first. 

cv.serial does this in extra work for clusterApply. 

Predicting the drag on a satellite clone the tpm repo

Think of clusterApply is something generic to the computing environment, will work on single machine

Or it will work on mulitple machines

OpenMP works only on a certain machine

Part of strategy is make a small version of your example and try to get that small version to work

Stragtegy for computing in that environemnt

Steve question: For problem 2, random number generation for the bootstrap, pragma omp atomic situation

Solution to getting random numbers in an environment, 
1. could ask for all random numbers in advance, v*n random numbers, and just reference them carefully in code
2. Can also do pragma omp, could use unif rand inside bootstrap, otherwise two threads could call ranomd unmber
3. Could change random number generators

Change the sleep function, use something different, use one of these :)

Generate the random numbers from within R

For 3 supposed to be about the same time. Hopefully it is a little easier, getting the calling environment, it is an end around solution, the Rcpp is very clean.

, v*n random numbers, and just reference them carefully in code
2. Can also do pragma omp, could use unif rand inside bootstrap, otherwise two threads could call ranomd unmber
3. Could change random number generators

Change the sleep function, use something different, use one of these :)





