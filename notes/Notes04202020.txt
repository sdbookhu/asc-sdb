## Notes from class on 4/20/2020

## Question about allocation

Don't need to do a very specific thing, goal for question is to use alot of computing power at once

In Bobby's he did all 32 cores of 5 nodes

Not possible to engage all 32, start with 2 nodes for an hour with 4 tasks on each node to make sure each code runs and then it finishes, not doing too many cross-validation folds

Creating an authentic experience

Ran overnight, didn't have to wait 24, but had to wait awhile, it's normal

Depends on load on computer

squeue --start -j then jobid

squeue -A -ascclass 

How much of the time they asked for, Erica and Sierra using 15 nodes, if use the entire request

Recommend not having two in the queue at one time, just run one at a time to be polite to classmate

3 reps and it didn't finish, 10 nodes each running 3 reps, then 30 total reps

Intention was 

Will run faster than the estimates

how n-copies relates to the number of reps we have

30 reps is 10 fold cv and his hope is to get that with a few hours of computation on many nodes at once

if Sierra is saving results, should be able to do them incrementally, many reps in RData files, won't have to start over

30 reps of 10 fold cv, on 10 nodes, each rep takes an hour, should take 3-4 hours

How choosing number of tasks on each node

Sierra asked for 4 ntasks, would guess could do with fewer nodes and do all 32 cores on each

5 or 6 copies on each node, each is using mkl for linear algebra

Ideally one nice clean script, but sometimes it isn't possible, build script in a way so you can pick up where it left off

If last one got cut off, can still create some new ones, and combine it with the old ones

Partial reps aren't useless, not ideal from cross validation, scratches the itch for the question

Annie asked about the number of things running at once, in the parallel script justin went through "n-copies" how that plays in 

Figured out how n-copies is 30, inside each reps 1, then gives 30 total reps

Bringing up Justin's script, spam_mc_parallel.sh -> spam_mc_parallel2.sh

## In that file

time at 24 minutes

Modifications that are important, thinking that we want to do multiple nodes, main points of the question

Start small, -N 2, goal to put 32 at --ntasks-per-node=8, barebones

Define a few variables in this script, only need to do 1 rep in the end

reps=1, nth = 4, set export MKL_NUM_THREADS = $nth

Determine number of copies based on available computing cores, instead of nparallel=8

pernode=$( $SLURM_CPUS_ON_NODE / $MKL_NUM_THREADS)

Calculate the ncopies instead of being 32 to $( $pernode * $SLURM_NNODES)

Going to need to know the names of the nodes that we are working on:

scontrol show hostname $SLURM_NODELIST > node.list 

This file will contain the nodes that have been assigned to the job

Give all the information to parallel, going to do ncopies load

4 copies of spam_mc.r, going to type in the parallel command

\\ allows to continue command on next file

seq 1 $ncopies | parallel \ --sshloginfile node.list \ -j$pernode

Need to tell parallel where to do the work

seq 1 $ncopies | parallel \
--sshloginfile node.list \
-j$pernode \
--workdir . \
--env R_LIBS \
"R CMD BATCH 

Needed to load R library on each node, need to do the same thing with each module, need to load the modules

module purge; module load parallel intel/18.2 mkl R/3.6.2; export MKL_NUM_THREADS=$nth; R CMD BATCH 

Running this on multiple nodes, found these by looking parallel help file, that it wasn't working the first times

Tell how many to run on each, where to do the work, need variable set for R

reps=$reps

## Annie question

Need to load the libraries that have the functions that we're calling on each machine on each parallel instance

putting spamfold into cluster apply

cls, that r instance is completely blank

Make sure you load the library before you use the functions from each

NEed to think of different compute nodes where the different cluster elements, need to think of them as blank slates, can load the packages, can send some amount of information in your call, need to build it up from nothing

Can do module save;

The modules you have as default, loads the modules

Similar to putting into the save or bash.rc file

Setting in parallel call is more temporary

## Talking about plotting, lecture 8

R has many tools for plots and graphs

Many cool ways to visualize things in r

Can control many ways of making a chart, R's graphics aren't very interactive

R's plotting philosophy is that generating graphics is the best by scripting

Better looking, reproducible and more linked to the underlying data

JSM 2009 hadley wickham, it's cool but it doesn't address a real problem

R can make as much as nice plots

ggplots as adding layers, calling data objects later

Downside, R plots are not interactive like plotly or others are

Scripts hard to read for interactive plotting

plot is a generic function, can plot many types of objects

Can give curve a formula and from scale

Prefer to make the grid ourselves, looking for oneliner on graph, maybe the curve command is what we want

identify if given x and y coordinates, with if click on some locations can add the labels, the text information from that variables, need to hit escape to return from it

Gets axis labels right and name the labels right, then can call text, must have a plot created to work



