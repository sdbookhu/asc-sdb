Notes from 3-23-2020

Groups of students worked together to research a topic in computing/R that he didn't require 

Were working on compiled code, doing this today and Wednesday. Friday Justin will be giving the lecture, will talk about running the spam example on the cluster on arc using more than one node

"Compiled.pdf" lecture

Wrote a faster C implementation, needed to compile the code

Called C functions by .C()

Need to build an R to C handshake, a rendezvous

mutlinks_R receives the code, ran it and it was alot faster in C

See the common themes and elements to get the program to work in R from C

Matrix vector operations heavily optimized within R.

State of the art linear algebra libraries, storing temporary calculations

R is slowest in making new vectors trying to copy information, opportunitites in C to make things faster.

NOTE: LOOK AT HW3 CODE TO ADJUST EXPLANATIONS

bootreg.R, icept

match.arg() -> checks if 

likes to have a separate file to test functions

Stress test the functions

R CMD SHLIB -o "clect.so" *.c

method = inv is the default way

lm is implemented in c, but it's doing more work in xtxxty

gives covariance matrix, doing a little bit more work

1:45pm

allocates space for m dimensional betahat, for ols

Now in C program, bootreg.c

Convert x, a vector, to a matrix

Comments very very important in C

dgemm - double precision generic matrix multiplication

pass in the matrix x as a vector, as a column vector, passing it as a vector anyway

C stores them as a column vector, need to give the dimensions

Very helpful for short functions, making identity matrices

Cholesky decomp and inv, xtx is replacd with the choelesky decomp

Since we know xtxinv is symmetric can save work by telling the program it doesn't need to look at lower triangle of matrix

1don't need to pre-allocate, can just put in betahat

Doing the same calculations, offloading to exactly the same subroutines, now we are being careful about memory. 

All of this is to set up for a fast bootstrap

BACK TO LECTURE SLIDE 17

When doing the compilation, R will read 

There's a main makevars in the R installation

When writing the C code, did not defin the dgemm, degemv, etc. functions. If R not set up with good linear algebra, then solutions would be much faster

Via: MKL, vecLib, automatically tuned libraries

In everyones mac, comes with vecLib

ATLAS: Automatically tuned linear algebra software

Intel has made computer, processor, computer board, they know the fastest computing

If you have any of the machines in the department

5 times faster to use the mkl library than the other linear algebra inverse

solving directly, .2 sections, just a different version of R by mkl

Microsoft R Open: they don't mention mkl, they compiled R with the mkl library and added a few extra functions.

Then microsoft bought in 2015, then released as R Open

Only support for windows and linux

What they were doing for apple, they were using the accelerate frame work

Run the three commands for accelerate framework, will swap out R built-in library, then making link between accelerate and what you moved out of the way. If restart R, will be much faster


 
