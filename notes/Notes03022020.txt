Notes from 3/2/2020

Talking about spam_mc.R

It's doing cross validation, going over all folds in cross validation

"transform" from the data module

Need to save often, partial results you want to see along the way, important to save the partial results  intermittantly

Big simulations, important to print out as you go

Can pick up from where it drops off.

Saving raw predictions from matrix, could save everything if you wanted

Getting the correct classification

Took several hours for one core, easiest way to take advantage is via shell

Could set up multiple Rstudio connections

Can run R scripts from the shell in batch without starting the interactive prompt, using R CMD BATCH

Don't want output files to trample one another

Some command line arguments, 

Goal is to be execute commands from shell, extra code for commandArgs to r CMD BATCH

Executing everything as code for the args

Can put inside function to overwrite other arguments, a little more simpler just copy and pasting one command

Dynamically giving work to processing unit

command called "renice", default nice is uniform for everyone

Change the nice level, give lower priority to your stuff

Easier for what we're trying to do is use the R CMD BATCH, could write out to files, easier than that, just having different lines of code

Need to collect output together, how to do that? Called spam_mc_collect.R

Search all folders within a folder?

using list.files()

Boxplots are nice visuals, can rank visuals by their average hit rate

Almost always put a comparison of boxplots

Basically just a paired t-test to see which ones are more statistically significant

Same data with different 1 sided t-test know what order that they are in, 

1. Want to RData don't benefit from versions, couldn't look at vrsioning
2. Not human readblae anyway, read in csv
3. Maximum of 1 gig, re commit it, will take however big the file is

Data that might be useful, save as csv, just don't put it on github, need to have multiple files, then use scp

Put in cloud, record of manual labor, not of computer labor, can always be regenerated

Save onto another disk, need plan in place to back-up everything

Unlimited google drive, duplicati (windows, mac, linux)

Unlimited storage while here, have something in place

BACKUP 
 
