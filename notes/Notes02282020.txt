Notes 02/28/2020

Monte Carlo & Parallelization

Alludes to games of chance played in casinos, repeated games in casinos to find the odds

idea of repeating something that has a random component, then writing down how many times something happens

MC need to set the iterations beforehand
MC integration is numerical integration using simulation

MC is a technique for replacing analytics with approximate computation

Investigate something through Monte Carlo, very valuable. 

Easy ways to turn computation to advantage, MC is one of those things

SLIDE ON BOX WITH INTEGRAL

Imagine throwing darts at the box, count the proportion in the gray area. 

By using more analytic structure

Exactly like expectiations, they are integrals

Get draws some other way, method of inversion, approach theoretical expectation

Anyone can guess something, not approximation until the variance can be estimated and the variance is decreasing over time

Can use variance of sample proportion to calculate the standard derorr of the estimate. 

Calculate area in some region

BOOTSTRAPPPPPPPPPP ----------------------------------------------------

Explore how the statiics change, all classifical statistical resolves, sensitivy to outliers

CLT gives an approximate answer analystically, don't need to be mathematically 	

General tool for assessing statistical accuracy. Re-sampling with replacement

Calculating any summary statistic or data

Complicated greek name for a calculation from drata, addup through somthing

Bootstrap is something gained from nothing

Resampling multiplicities, random multinomial weight, redoing over and overagain

Many bootstraps are small, B calculations of statistics from the non-parametric distribution, 

Bootstrap replications used to summarized distribution, resampling indices

Estimate any sample statistic

MC method, estimate through empirical data

Data come from distribution, doesn't matter what it is

BOOTSTRAP REGRESSIO

Sample X's and Y's together, then computing least dsue estimates

More similar in some entries compared to others

Sampling dist for betahat in classical ense, it condition on x for y|x

Only conditional dist for y|x. Bootstrap is resampling both syt the same time

Just wanna do least squares of y|x

MONTE CARLO BAKEOFF ------------------------------

Validation exercises, randomly generate training and test sets

Have an idea for predicting system, have to compare to what other people do

Hard part is that many predictors are computationally demanding

Variablity in behavior, sometimes called a horse race too compared to a bakeoff

CROSS VALIDATION ----------------------------------------------------------------

Estimates generalization error, 5 equally sized sets, loop over each fold in partition

cv.folds <- function(n, folds=10) {
split(sample(1:n), rep(1:folds, length=n))
}

shuffles them up, splits equally sized groups
