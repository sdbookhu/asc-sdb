Parallelizing, going over the data and number of reps/cross-validation

Parallelizing for simulation settings, enumerate the settings

Anytime doing apply command, can do on a cluster as well, need to first set up what the cluster is

Need to point to the nodes, parfor doing make cluster for you

In theory, can ssh into, can use as element of the cluster, practice very hard to do

Machines are heterogeneous, alot easier than the alternative than making the commands

Need way to connect to it, that it is listening for that connect

Steve is on board, every linux and mac machine available for parallelization

All lab machines used to be all together, could establish structure

## Class

Using a regression line with geom_smooth()

ggplot has built in statistical methods, are outsourcing to other methods 

plot_ly is pretty cool, Bobby likes

Easy to publish interactive graphics on the web, can add extra attributes to plotly

Can work with plotly with an account, can have a library of plots, then make the plots easily shareable

Wanna share with colleague? Send html file or send link to web page hosted by plotly

Tell R the api key and plotly credentials

Online graph maker, api key important, in lieu of username and password

Some people put these R files

Can convert ggplot to plotly, ggplotly()

Can modify with, plotly comes with data set economics, similar to homework

Can convert ggplot to plotly fairly easily

Can see 3D plot in plotly

plotly is super cool, offload to javascript, interactive, easy to share, potentially very beautiful plots


