Notes for class 2/10/20

Setting up batch jobs in a unix environment.

ARC useful for big batches, can run on other linux machines that we have access to

Get in the habit of working with R differently

For big jobs, just want to set it running and leave it alone

Can use source("thisfile.R"), important to put print statements in for big jobs

Having a save command in the for loop, need to get partial results if possible

Better way is command line itself

We can also run in the background, using an &

When you logout, it kills all processes automatically on default

Justin's gonna show us how to run this on arc

You have a request resources so they can be allocated to you

-----------------------------------------------------------------------------------------------

Back to lecture:

Environments, stores symbol mappings in hash tables, can list objects in your environment

Other environments created that are higher than the global environment, all environments chain back to empty environment

Stack of environments, 
global -> parent ->       ---------> empty

Layers of code that R code is running in, how it interfaces with R studio

Call a new function f, you go into that environment, assigned symbols in the local argument

recursion calls same environment everytime

Get random forest package, package environment is the parent

Able to access stack of calling environments

parent.frame() allows you to access the calling environment

eval() can evaluate a function in any environment

Write a function that accesses it directly

lm is sent formula, pulls variables from the calling environment via parent.frame()

Really good to use this environment in optimization

Evaluate and approximate its gradient, better to have one function do both things in the parent environment

Doubling up effort, can use intermediate effort

with function: with(df, a+b+c), allows access to symbols in the environment 

Most optimizers evaluate the function and to approximate the gradient, to get a tangent line

<< goes up a stack, magic iterations over environments

ARC on Wednesday and Friday
