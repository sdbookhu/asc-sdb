Notes from 4/27/2020

On homework 6, calculate returns

Use the returns to calculate the dow jones industrial average

Correlation of stocks returns, 30 time series 

Will want to have these stocks to calculate the correlations

What is b asking? 

It makes it possible to compare these numbers over time, basically just summing up prices, can divide by number of stocks
Or can divide by whatever they want

Treats all companies as equals, they do more business and they impact more industries

Use the sizes as the weights, S&P500 is a larger group, not asking to do all 300, just use the 30 from dow

Going through ggplot and plotly

Idea is to have plots built up in layers, more intimately tied in with data

Tidyverse of working with R, hadley wickham phd student at iowa state, developed dplyr and data table

Almost like a different language, similar to other types of programming in other languages

So just calling the first ggplot(data=baseball, aes(x=...,y=...)), what is it you want to plot on

aes looks like a function, that function doesn't work really on its own, it doesn't know what the varibles are

Idea is to save this, defined an S3 method, going to call one of those methods

Print isn't writing something to the terminal it's just writing to graphics device

geom_point(), geom_line()

You can accumulate a plot through an R object, the plot is stored as an object rather than a sequence of commands

ggplot on paper, uses alot of ink to fill white space with grey

Tidyverse: dplyr, also advocating that piping works well too

Can pass a data arg into the diff and then in mean

If just using the first arguments of the functions

Easy to add layers by variables

The R thing would be to make a new vector of color reference values, difference is using vectors in R
Saving intermediate data to put in plots, then ggplot is adjusting the plotting object

qplot() is like the r version plot in ggplot

Have to write to png device, 

Getting old device, just a wrapper around that, then closing it down, ggsave is the command


