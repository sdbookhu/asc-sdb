## Title: README for hw6 folder

This folder contain all pertinent files related to hw6. (besides unix.txt)

### hw6.Rmd: This is the base Rmd file, which compiled creates hw6.pdf

### nhlteams.csv: Given datatable that contains names/references to NHL teams

### nhlstandings.R: Rscript that prints out the nhl teams sorted by points, wins, etc.

### nhlstandings.sh: Bash script that calls nhlstandings, but a user can specify "central, slb, etc." to specify which portion of the table they want to see.
