#!/bin/bash

stringg=$1

if [ $# -eq 0 ]
then 
	stringg="all"
else
	stringg=$1
fi

Rscript nhlstandings.R "filt='$stringg'"

#If you'd like to run this to a file use below command

#R CMD BATCH  "--args filt='$stringg'" nhlstandings.R &
