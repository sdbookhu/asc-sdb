Title: README.md

Author: Shane Bookhultz

Purpose: To explain the directory contents of hw1

In this folder, you will find one file, the hw1.Rmd file. This file contains all answers to question 3 in hw1. I already have completed questions 1 and 2, but for brevity sake, I will not include those questions in the Rmarkdown file.

Note: You will need to install package "formatR" to run this file. 
