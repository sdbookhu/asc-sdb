#!/bin/bash

# Sample submission script for snow Metropolis-Hastings
# Make sure to change the -A line to your allocation

#SBATCH -t 10:00
#SBATCH -p normal_q
#SBATCH -A ascclass
#SBATCH -N2 --ntasks-per-node=8

# Add modules
rver="3.6.1" #R version
module purge
module load intel/18.2 mkl R/$rver openmpi/4.0.1 R-parallel/$rver

export OMP_NUM_THREADS=$SLURM_NTASKS_PER_NODE

# OpenMPI environment variables

export OMPI_MCA_btl_openib_allow_ib=1 #allow infiniband
export OMPI_MCA_rmaps_base_inherit=1  #slaves inherit environment

# Run R
mpirun -np 1 --oversubscribe Rscript mh_snow.r

exit;
