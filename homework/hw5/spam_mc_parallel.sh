#!/bin/bash

#SBATCH -N 4
#SBATCH --ntasks-per-node=8
#SBATCH -t 10:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

cd $SLURM_SUBMIT_DIR

module purge
module load parallel
module load intel/18.2 mkl R/3.6.1 openmpi/4.0.1 

#set r libraries
#export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

export R_LIBS="/opt/apps/intel18_2/openmpi4_0/R-parallel/3.6.1/lib"

#set number of cores used by each r process
reps=2
nth=2

export MKL_NUM_THREADS=$nth

#Processes to run on a single node
pernode=$(( $SLURM_CPUS_ON_NODE / $MKL_NUM_THREADS ))

#Processes to run total
ncopies=$(( $pernode * $SLURM_NNODES ))

scontrol show hostname $SLURM_NODELIST > ./node.list

echo "$( date ): Starting spam_mc"

# for i in $( seq 1 $ncopies ); do 
#   R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout &
# done
# wait
#seq 1 $ncopies | parallel \
#	--sshloginfile node.list \
#	-j$pernode \
#	--workdir . \
#	--env R_LIBS \
#	--env MKL_NUM_THREADS \
#	"module purge; module load parallel intel/18.2 mkl R/3.6.2; 
#	export MKL_NUM_THREADS=$nth;
#	R CMD BATCH \"--args seed={} nth=$nth reps=$reps\" spam_snow.R spam_snow_{}.Rout"




#mpirun -np $SLURM_NNODES --map-by ppr:$pernode:node --oversubscribe R CMD BATCH spam_snow.R spam_snow.Rout 

mpirun -np $SLURM_NNODES --oversubscribe R CMD BATCH spam_snow.R spam_snow.Rout 


echo "$( date ): Finished spam_mc"
exit;
#mpirun --map-by ppr:$pernode:node 

#echo "$( date ): Starting spam_mc_collect"
#R CMD BATCH spam_mc_collect.R spam_mc_collect.Rout
#echo "$( date ): Finished spam_mc_collect"

