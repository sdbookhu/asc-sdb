## Title: README.md

## Author: Shane Bookhultz

This hw5 folder contains all needed files to run hw5.Rmd

All files with ending of .PNG (or .png) are screenshots used in the hw5.Rmd document

mcpi_snow* files implement the rmpi version of calculating pi in problem 1.

mh_snow* files implement the metropolis-hasting example for problem 1.

sat_laGP* and sat_grace* files have the data and code for running problem 2.

spam.csv is the underlying data for problem 3. 

spam_mc.R and s_mc.sh/s_mc2.sh run the problem 3a, GNU parallel portion, which also called spam_mc_collect2.R, which compiles all the spam.RData files into one. version 1 and 2 have the same base script, but have different allocations for nodes, threads, and time.

Prob3a.csv is the output of spam_mc.sh, and is used for creating a boxplot of the hit rates for GNU parallel

spam_snow.R and spam_mc_parallel.sh run problem 3b, the rmpi portion.
