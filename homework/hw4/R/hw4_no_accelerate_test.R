## Shane Bookhultz
## This function will record all the "slow" times for the functions 
## in problems 1 and 2

## Problem 1
## I'm going to save the likelihood vectors for all 4 methods

set.seed(5)
source("mvnllik.R")
## Setting up the data

thetas <- seq(0.1, 3, length=100)
X <- runif(300, -1, 1)
D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
library(mvtnorm) 
Y <- rmvnorm(10000, sigma=Sigma)  

## 1a, logliks with no openmp parallel

#C_time <- system.time({llC <- logliks(Y, D, thetas, verb=0, name="logliks_R1a")})
#maxll <- which.max(llC)

#plot(thetas, llC, type="l", main=paste0("Loglikelihood across theta values, max at theta=",round(thetas[maxll[1]],2)))
#abline(v=thetas[maxll[1]])

## 1b, parallel openmp 

#C_OMP_time <- system.time({llC_OMP <- logliks(Y, D, thetas, verb=0, name="logliks_R1b")})

## 1c, other two R versions

#R1_time <- system.time({llR1 <- logliks.R(Y, D, thetas, verb=0, ll=loglik)})
#R2_time <- system.time({llR2 <- logliks.R(Y, D, thetas, verb=0, ll=loglik2)})

#png(filename="./NoAccelerateProblem1.png")
#par(mfrow=c(2,2))
#plot(thetas, llC, type="l", main= "Loglik C without OPENMP", ylab="ll")
#plot(thetas, llC_OMP, type="l", main = "Loglik C with OPENMP", ylab="ll")
#plot(thetas, llR1, type="l", main= "Loglik R version 1", ylab="ll")
#plot(thetas, llR2, type="l", main= "Loglik R version 2", ylab="ll")
#dev.off()

## Now save the data

#prob1times <- rbind(C_time[1:3], C_OMP_time[1:3], R1_time[1:3], R2_time[1:3])
#rownames(prob1times) <- c("C w/o openmp", "C w/ openmp", "R v1", "R v2")
#write.csv(prob1times, file = "./NoAccelerateProblem1times.csv")

#############
# Problem 2
#############

#source("bootreg.R")
if(FALSE) {

n <- 5000
d <- 200
X <- 1/matrix(rt(n*d, df=1), ncol=d)
beta <- c(1:d, 0)
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)


## testing bootstrapped OLS
Rlm <- system.time(boot.Rlm <- bootols.R(X, Y, B=1000, method="lm"))
Rinv <- system.time(boot.Rinv <- bootols.R(X, Y, B=1000, method="inv"))
Rsolve <- system.time(beta.Rsolve <- bootols.R(X, Y, B=1000, method="solve"))
Rcp <- system.time(beta.Rcp <- bootols.R(X, Y, B=1000, method="cp"))
Cinv <- system.time(boot.Cinv <- bootols(X, Y, B=1000, inv=TRUE, name="bootols_R"))
Csolve <- system.time(beta.Csolve <- bootols(X, Y, B=1000, name = "bootols_R"))

## Parallel in C
Cparinv <- system.time(beta.Cparinv <- bootols(X, Y, B=1000, inv=TRUE, name = "bootols_ompR"))
Cparsolve <- system.time(beta.Cparsolve <- bootols(X, Y, B=1000, name = "bootols_ompR"))

prob2times <- rbind(Rlm[1:3], Rinv[1:3], Rsolve[1:3], Rcp[1:3],
		    Cinv[1:3], Csolve[1:3], Cparinv[1:3], Cparsolve[1:3])
rownames(prob2times) <- c("Rlm", "Rinv", "Rsolve", "Rcp", 
			  "Cinv", "Csolve", "Cparinv", "Cparsolve")
write.csv(prob2times, file = "./NoAccelerateProblem2times.csv")

## Testing problem 3

library(Rcpp)
sourceCpp(file = "./../src/swap.cpp")
v <- as.numeric(1:1000000000)
i <- 1; j <- 2
t1.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))
t2.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))
t3.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))

swap.eval <- function()
{
	e <- quote({tmp <- v[i]; v[i] <- v[j]; v[j] <- tmp})
	eval(e, envir=parent.frame())
}

t1.s.eval <- system.time(swap.eval())
t2.s.eval <- system.time(swap.eval())
t3.s.eval <- system.time(swap.eval())

presprob3 <- round(rbind(t1.s.Rcpp[1:3], t2.s.Rcpp[1:3], t3.s.Rcpp[1:3],
			 t1.s.eval[1:3], t2.s.eval[1:3], t3.s.eval[1:3]),3)

rownames(presprob3) <- c("Rcpp 1", "Rcpp 2", "Rcpp 3", "R eval 1", "R eval 2", "R eval 3")
write.csv(presprob3, file = "./NoAccelerateProblem3times.csv")
}
## Problem 4
library(Rcpp)
thetas <- seq(0.1, 3, length=100)
X <- runif(300, -1, 1)
D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
library(mvtnorm) 
Y <- rmvnorm(10000, sigma=Sigma)  


library(RcppArmadillo)
sourceCpp("./../src/mvnllik.cpp")

## Using the Y, D, thetas from above...
llik_rcpp <- rep(NA, length(thetas))
zerovec <- rep(0, 300)
t1.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})
#print(t.ll.rcpp)


t2.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})


t3.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})

presprob4 <- round(rbind(t1.ll.rcpp[1:3], t2.ll.rcpp[1:3], t3.ll.rcpp[1:3]),3)
write.csv(presprob4, file = "./NoAccelerateProblem4times.csv")

png(filename = "./NoAccelerateLoglikcurve4.png")
plot(thetas, llik_rcpp, type="l", main="Rcpp Loglike curve")
dev.off()
