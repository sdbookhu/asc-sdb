---
title: "Homework 4"
subtitle: "Advanced Statistical Computing (STAT 6984)"
author: "Shane Bookhultz (<sdbookhu@vt.edu>) <br> Department of Statistics, Virginia Tech"
output: pdf_document
---


## Instructions

This homework is **due on Wednesday, April 10th at 1:25pm** (the start of class). Please turn in all your work. The primary purpose of this homework is explore `C` and `OpenMP` in R.  This description may change at any time, however notices about substantial changes (requiring more/less work) will be additionally noted on the class web page.  Note that there are two prongs to submission, via Canvas and Bitbucket (in `asc-repo/hwk/hw4`).  You don't need to use `Rmarkdown` but your work should be just as pretty if you want full marks.


## Problem 1: Multivariate Normal Likelihood (35 pts)


Consider a multivariate normal generating mechanism defined in terms of a so-called lengthscale parameter $\theta$

$$
Y \sim \mathcal{N}_n (0, \Sigma) \quad \mbox{where} \quad \Sigma_{ij} = \exp \left\{- D_{ij}/\theta \right\}
$$

and the $n \times n$ matrix $D$ contains pairwise (Euclidean) distances between $Y_i$ and $Y_j$ values in a predictor, or $X$-space.  

- Treat $\theta$ as a positive scalar quantity.
- Such a mechanism comprises the essence of Gaussian process regression.

Our goal is to infer the unknown $\theta$ parameter from data by evaluating the likelihood for a grid of $\theta$ values

```{r}
thetas <- seq(0.1, 3, length=100)
```

using data generated as 

```{r, eval = T}
X <- runif(300, -1, 1)
D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
library(mvtnorm)
Y <- rmvnorm(10000, sigma=Sigma)
```

and report on the MLE.

- The functions in [`mvnllik.R`](mvnllik.R) provide two versions (coded in R with and without the help of external libraries) of evaluating the likelihood via the MVN density function, and it also contains a wrapper function iterating over a vector of potential $\theta$ values.
- The functions is [`mvnllik.c`](mvnllik.c) provide analogues in C.

Your task is to 

a. Develop a C/R interface in order to utilize the C version from R, defining the function `logliks` in R, which takes the same arguments as `logliks.R` excepting `ll`.

```{r test out llik, eval = T}
set.seed(5)
source("mvnllik.R")

C_time <- system.time({llC <- logliks(Y, D, thetas, verb=0, name="logliks_R1a")})
maxll <- which.max(llC)

plot(thetas, llC, type="l", main=paste0("Loglikelihood across theta values, max at theta=",round(thetas[maxll[1]],2)))
abline(v=thetas[maxll[1]])

```

The MLE for theta is shown above at the vertical line. 

b. Extend the C version to utilize `OpenMP` parallelization.  There are at least two opportunities to do this, but you only need to do one

```{r show openmp results, eval = T}

C_OMP_time <- system.time({llC_OMP <- logliks(Y, D, thetas, verb=0, name="logliks_R1b")})

```

I used `#pragma omp parallel for private(t)` with the function logliks_R1b, which in C refers to logliks_1b. 

c. Show that all four versions give the same likelihood curve, and maximizing value, over the $\theta$ grid.  I.e., show that they give exactly the same output, demonstrating correctness.

I have created one function in R, logliks, which can input the name of which function we need to call. One being the openmp version, and one not being the openmp version


```{r showing correctness, eval = T}

R1_time <- system.time({llR1 <- logliks.R(Y, D, thetas, verb=0, ll=loglik)})

R2_time <- system.time({llR2 <- logliks.R(Y, D, thetas, verb=0, ll=loglik2)})

par(mfrow=c(2,2))

t1 <- round(thetas[which.max(llC)],3)
t2 <- round(thetas[which.max(llC_OMP)],3)
t3 <- round(thetas[which.max(llR1)],3)
t4 <- round(thetas[which.max(llR2)],3)


plot(thetas, llC, type="l", main= paste("Loglik C without OPENMP, MLE =",t1), ylab="ll")
abline(v=t1)
plot(thetas, llC_OMP, type="l", main = paste("Loglik C with OPENMP, MLE =",t2), ylab="ll")
abline(v=t2)
plot(thetas, llR1, type="l", main= paste("Loglik R version 1, MLE =",t3), ylab="ll")
abline(v=t3)
plot(thetas, llR2, type="l", main= paste("Loglik R version 2, MLE =",t4), ylab="ll")
abline(v=t4)

```

Based on the four above plots. We can see that they have the same exact MLE for theta, with likelihoods obtained from 4 separate functions.

d. Provide a timing comparison between the four versions.  Be sure to note how many cores your parallel `OpenMP` version is using.

```{r timing comparison, eval= T}
## Now create a table

presmat <- rbind(C_time[1:3], C_OMP_time[1:3], R1_time[1:3], R2_time[1:3])
cores <- c(1,8,1,1)
presmat <- cbind(presmat,cores)
rownames(presmat) <- c("C w/o openmp", "C w/ openmp", "R v1", "R v2")
library(knitr)
kable(presmat, caption = "Comparison of times to calculate loglikelihood")
```

Above is a table of time comparisons between the R methods. We can see that the C version with OPENMP takes the least amount of time as it is using 8 cores. The regular C version, most comparable to R version 2, is taking around the same amount of time as R version 1. Overall, R version two takes the most amount of time.

e. Now, using an R with accelerated linear algebra (Intel MKL, OSX Accelerate, or Microsoft R/MRAN), re-perform your comparison from d. and report the results.  (If your timings are not *all* much faster then you're doing it wrong.)

Since I compiled this Rmd document in R MKL, I will show the timings from the non-accelerated linear algebra R. In this comparison, I will only be looking at elapsed times. 

```{r compare between acc and non 1}
non_acc <- read.csv("./no_accelerate/NoAccelerateProblem1times.csv")

## Let's look 

allp1times <- matrix(NA,nrow=4,ncol=2)
allp1times[,1] <- round(non_acc$elapsed,3)
allp1times[,2] <- round(presmat[,3],3)
rownames(allp1times) <- rownames(presmat)
colnames(allp1times) <- c("R elapsed", "R MKL elapsed")

kable(allp1times, caption = "Comparison for mvnllik between regular R and accelerated R")

```

As suggested above, all of the timings are much faster using accelerated linear algebra than not. It seems like the C version without OpenMP achieved the greater amount of speed up, about 14 times in magnitude. We see similar trends though, as the C version with OpenMP takes the least amount of time.


## Problem 2: Parallel bootstrapped linear regression (25 pts)

Parallelize `bootreg` with `OpenMP` and report timings comparing your new parallel version to the R and `C` versions provided in class.

- Your comparison should include both ordinary R and a version with accelerated linear algebra.

Here are some helpful suggestions/observations.

- I recommend using "`#pragma omp parallel`" rather than "`#pragma omp parallel for`".
- Be careful to ensure that the parallelized instances (threads) are actually algorithmically independent, which may require introducing some auxiliary storage.
- R's random number generator (RNG) is not "thread safe", which means that if two threads ask for a random deviate at the same time, they might get the same number (i.e., not very random).  How can you work around that without compromising efficiency?

In order to ensure that the parallelized instances are algorithmically independent, I have declared all my variables within the `#pragma omp parallel` to ensure algorithmic independent instances. Since R's RNG is not thread safe, I sample all my random bootstrap indices in advance to prevent any type issues with thread safety. Down below I compare times between the regular R version of all 8 of these functions, computed offline, and the accelerated linear algebra times computed in real time.  

```{r test bootreg, eval = T}
source("bootreg.R")
n <- 5000
d <- 200
X <- 1/matrix(rt(n*d, df=1), ncol=d)
beta <- c(1:d, 0)
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)

Rlm.mkl <- system.time(boot.Rlm <- bootols.R(X, Y, B=1000, method="lm"))
Rinv.mkl <- system.time(boot.Rinv <- bootols.R(X, Y, B=1000, method="inv"))
Rsolve.mkl <- system.time(beta.Rsolve <- bootols.R(X, Y, B=1000, method="solve"))
Rcp.mkl <- system.time(beta.Rcp <- bootols.R(X, Y, B=1000, method="cp"))
Cinv.mkl <- system.time(boot.Cinv <- bootols(X, Y, B=1000, inv=TRUE))
Csolve.mkl <- system.time(beta.Csolve <- bootols(X, Y, B=1000))

## Parallel in C
Cparinv.mkl <- system.time(beta.Cparinv <- bootols(X, Y, B=1000, inv=TRUE, name = "bootols_ompR"))
Cparsolve.mkl <- system.time(beta.Cparsolve <- bootols(X, Y, B=1000, inv=TRUE, name = "bootols_ompR"))

## example of bootstrap samples
par(mfrow=c(2,2))
hist(beta.Cparinv[,1])
hist(beta.Cparsolve[,1])
hist(beta.Cparinv[,20])
hist(beta.Cparsolve[,20])

```

Based on the above histograms, we can see that they are resampling correctly and that the two samples are not identical.


```{r compare times in bootreg}
prob2times.mkl <- rbind(Rlm.mkl[1:3], Rinv.mkl[1:3], Rsolve.mkl[1:3], Rcp.mkl[1:3], Cinv.mkl[1:3],
			Csolve.mkl[1:3], Cparinv.mkl[1:3], Cparsolve.mkl[1:3])
rownames(prob2times.mkl) <- c("Rlm.mkl", "Rinv.mkl", "Rsolve.mkl", "Rcp.mkl", 
			      "Cinv.mkl", "Csolve.mkl", "Cparinv.mkl", "Cparsolve.mkl")
kable(prob2times.mkl, caption = "MKL time comps for bootreg")

rm(boot.Rlm, boot.Rinv, beta.Rsolve, beta.Rcp, boot.Cinv, beta.Csolve, beta.Cparinv, beta.Cparsolve) 

allp2times <- matrix(NA, nrow=8, ncol=2)

non_acc2 <- read.csv("./no_accelerate/NoAccelerateProblem2times.csv")

allp2times[,1] <- round(non_acc2$elapsed,3)
allp2times[,2] <- round(prob2times.mkl[,3],3)
rownames(allp2times) <- c("Rlm", "Rinv", "Rsolve", "Rcp", "Cinv", "Csolve", "Cparinv", "Cparsolve")
colnames(allp2times) <- c("R elapsed", "R MKL elapsed")

kable(allp2times, caption = "Comparison for bootreg between regular R and accelerated R")

```

Overall, the parallelized C versions with accelerated linear algebra take the shortest amount of time. We can see this, since the machine I used has 8 threads of processing power to use. It seems like it is definitely more efficient to program these calculations in C because R has its limitations in speed ups. As seen in question 2, the accelerated framework seemed to significantly improve the speed of all functions. In the regular R version though, it seems like the Rsolve and Rcp speed up these computations more than C does.  


## Problem 3: `Rcpp` for `swap` (15 points)

Revisit the swap function from problem 2 of [homework 2](hwk2.html) and write a version in `Rcpp` which, when called as `swap.Rcpp(v, i, j)` swaps entry `i` and `j` without copying the vector `v`.  Demonstrate correctness, and compare timings with our fast `swap.eval`.

```{r cpp problem 3, eval = T}
library(Rcpp)
sourceCpp(file="./../src/swap.cpp")
v <- as.numeric(1:1000000000)
i <- 1; j<- 2
t1.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))
print(ss[1:3])
rm(ss)
t2.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))
rm(ss)
t3.s.Rcpp <- system.time(ss <- swap_Rcpp(v, i, j))
rm(ss)
## the swap function from the previous homework

swap.eval <- function()
{
	e <- quote({tmp <- v[i]; v[i] <- v[j]; v[j] <- tmp})
	eval(e, envir=parent.frame())
}

t1.s.eval <- system.time(swap.eval())
t2.s.eval <- system.time(swap.eval())
t3.s.eval <- system.time(swap.eval())

presprob3 <- round(rbind(t1.s.Rcpp[1:3], t2.s.Rcpp[1:3], t3.s.Rcpp[1:3],
		   t1.s.eval[1:3], t2.s.eval[1:3], t3.s.eval[1:3]),3)

rownames(presprob3) <- c("Rcpp 1", "Rcpp 2", "Rcpp 3", "R eval 1", "R eval 2", "R eval 3")
suppressWarnings(library(knitr))
kable(presprob3, caption = "Timing comparison for Swap function in R MKL")
rm(v)

allp3times <- matrix(NA, nrow=6, ncol=2)

non_acc3 <- read.csv("./no_accelerate/NoAccelerateProblem3times.csv")

allp3times[,1] <- non_acc3$elapsed
allp3times[,2] <- presprob3[,3]

rownames(allp3times) <- rownames(presprob3)
colnames(allp3times) <- c("R elapsed", "R MKL elapsed")

kable(allp3times, caption = "R and R MKL time comparisons for swap")


```

As seen above, the accelerated linear algebra doesn't really help in this swap function. Interesting enough, both functions are saved locally in R, and thus when one repeats the swap, it takes no time at all. 


## Problem 4: `Rcpp` for `mvnllik` (25 pts)

Write an `Rcpp` version of `logliks`, called `logliks.Rcpp` which takes the same arguments.  You may do this one of two ways, both for extra credit:

- as a wrapper around the `C` function `logliks`;
- as a `C++` version of the R function `logliks2`.

Demonstrate correctness (i.e., show that your new version gives the same answers as the old ones) and compare timings with the other methods.

Using the Rcpp version of `mvnllik`(https://gallery.rcpp.org/articles/dmvnorm_arma/), down below I show how this function returns the same answers as the previous method. I have modified the dmvnorm_arma function in `mvnllik.cpp` to take in a distance matrix and a theta value to match the logliks2 code used above.

```{r Rcpp mvnllik, eval = T}
library(Rcpp)
library(RcppArmadillo)
X <- runif(300, -1, 1)
D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
library(mvtnorm)
Y <- rmvnorm(10000, sigma=Sigma)

sourceCpp("./../src/mvnllik.cpp")

## Using the Y, D, thetas from above...
llik_rcpp3 <- llik_rcpp2 <- llik_rcpp1 <- rep(NA, length(thetas))
zerovec <- rep(0, 300)

t1.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp1[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})

t2.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp2[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})

t3.ll.rcpp <- system.time({
	for(i in 1:length(thetas)) {
		llik_rcpp3[i] <- sum(logliks_Rcpp(Y, zerovec, D, thetas[i], logd=TRUE))
	}
})

#print(t1.ll.rcpp)
#print(t2.ll.rcpp)
#print(t3.ll.rcpp)

t1.rcpp <- round(thetas[which.max(llik_rcpp1)],3)
t2.rcpp <- round(thetas[which.max(llik_rcpp2)],3)
t3.rcpp <- round(thetas[which.max(llik_rcpp3)],3)

par(mfrow=c(1,3))
plot(thetas, llik_rcpp1, type="l", main=paste("Rcpp Loglike curve, MLE = ",t1.rcpp))
abline(v=t1.rcpp)
plot(thetas, llik_rcpp2, type="l", main=paste("Rcpp Loglike curve, MLE = ",t2.rcpp))
abline(v=t2.rcpp)
plot(thetas, llik_rcpp3, type="l", main=paste("Rcpp Loglike curve, MLE = ", t3.rcpp))
abline(v=t3.rcpp)

non_acc4 <- read.csv("./no_accelerate/NoAccelerateProblem4times.csv")

allp4times <- matrix(NA,nrow=3,ncol=2)

allp4times[,2] <- round(rbind(t1.ll.rcpp[3], t2.ll.rcpp[3], t3.ll.rcpp[3]),3)
allp4times[,1] <- non_acc4$elapsed

rownames(allp4times) <- c("Rcpp Rep 1", "Rcpp Rep 2", "Rcpp Rep 3")
colnames(allp4times) <- c("R elapsed", "R MKL elapsed")

allp4times <- rbind(allp4times, allp1times)

kable(allp4times, caption = "Timing comparison for all mvnllik functions") 
```

In the three graphs, three repetitions of the logliks_Rcpp function, give the same MLE theta value. Looking at the times, it seems that the Rcpp version takes slightly longer than both C versions and the R version 1. While we achieved quite good speedup with MKL, it is not faster than the C version. I think that the Rcpp version runs slower because of time needed to go between languages and might involve more memory allocation, although I would need to check that via a C profiler. 

