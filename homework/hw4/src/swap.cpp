#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector swap_Rcpp(NumericVector v, int i, int j) {
	// Converting R indices to C indices
	//double* ptrv = &v(0);
	int tmp;
	tmp = v[i-1];
	v[i-1] = v[j-1];
	v[j-1] = tmp;
	return v;
}
