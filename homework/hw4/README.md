# Title: README.md

## Author: Shane Bookhultz

This hw4 folder contains all the necessary files to run all 4 problems in this homework. There are two folders, R and src, to contain all the source code C files and R files (including the Rmd document)

## R folder:

### bootreg.R: Contains all R functions and R to C handshake for question 2. 

### hw4.Rmd: Main document to be compiled, makes calls to R and src files.

### hw4_no_accelerate_test.R: R file that when compiled, creates 4 csvs files for a time comparison between accelerate and regular R (non-parallelized linear algebra). 

### mvnllik.R: Contains all R functions and R to C handshake for question 1.

### no_accelerate: Storage folder for all time comparisons for all 4 questions. Output from hw4_no_accelerate_test.R.

## src folder:

### bootreg.c & bootreg.o: Main C file to run the backend code for bootreg.R functions. When using this in R, please load the hw4_2.so shared object

### hw4_1.so and hw4_2.so: Shared objects to load into R (compiled C code), to run questions 1 and 2 respectively.

### Makevars: A Makevars file to compile the shared objects hw4_(1,2).so. Contains flags for OpenMP.

### mvnllik.c and mvnllik.o: Main C file to run the backend code for the mvnllik.R functions. When using this in R, please load the hw4_1.so shared object.

### mvnllik.cpp: C++ code corresponding to question 4 in the homework. Runs the Rcpp function for calculating a mvn loglikelihood. 

### swap.cpp: C++ code corresponding to question 3 in the homework. Runs the Rcpp function for swapping two elements in a vector.
