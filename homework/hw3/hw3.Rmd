---
title: "Homework 3"
subtitle: "Advanced Statistical Computing (STAT 6984)"
author: "Shane Bookhultz (<sdbookhu@vt.edu>) <br> Department of Statistics, Virginia Tech"
output: pdf_document
---


## Instructions

This homework is **due on Wednesday, March 25th at 1:25pm** (the start of class). Please turn in all your work. The primary purpose of this homework is to hone debugging and profiling skills and explore parallelization with Monte Carlo experiments.  This description may change at any time, however notices about substantial changes (requiring more/less work) will be additionally noted on the class web page.  Note that there are two prongs to submission, via Canvas and Bitbucket (in `asc-repo/homework/hw3`).  You don't need to use `Rmarkdown` but your work should be just as pretty if you want full marks.


## Problem 1: Profiling (20 pts)


Below are two, very compact, versions of functions for calculating powers of a matrix.  

```{r}
powers3 <- function(x, dg) outer(x, 1:dg, "^")
powers4 <- function(x, dg) 
 {
   repx <- matrix(rep(x, dg), nrow=length(x))
   return(t(apply(repx, 1, cumprod)))
 }
```

a.  First, briefly explain the thought process behind these two new methods.

The thought process behind the powers3 method is that it takes the outer product of all different degrees, which means it calculates the outer product of the matrix "degree" times. This takes each element in the matrix and takes the power of each one. For powers4, the first line, repx, just sets up a starting matrix of x's. Then, the cumprod function takes each row of that matrix, cumulatively taking the product for each row, so the first column would have the 1st power, second would have 2nd power, and so on. Before I test times, it seems like the powers3 function will take a shorter amount of time since it doesn't need to copy a vector multiple times.  

b.  Then provide a summary of the computation time for all four versions (two from lecture and the two above).  Use the same `x` from lecture.

```{r}
x <- runif(10000000)
```

```{r lecture powers}
## Powers function 1 from the notes
powers1 <- function(x, dg) {
   pw <- matrix(x, nrow=length(x))
   prod <- x
   for(i in 2:dg) {
      prod <- prod * x
      pw <- cbind(pw, prod)
   }
   return(pw)
}
## This function allows for pre-allocation of size for vectors
powers2 <- function(x, dg) {
   pw <- matrix(nrow=length(x), ncol=dg)
   prod <- x ## current product
   pw[,1] <- prod
   for(i in 2:dg) {
      prod <- prod * x
      pw[,i] <- prod
   }
   return(pw)
}
```

```{r summary times}

times1 <- system.time({p1 <- powers1(x, 16)})
times2 <- system.time({p2 <- powers2(x, 16)})
times3 <- system.time({p3 <- powers3(x, 16)})
times4 <- system.time({p4 <- powers4(x, 16)})

presdf <- rbind(times1, times2, times3, times4)
rownames(presdf) <- c("Powers 1", "Powers 2", "Powers 3", "Powers 4")

## For printing out time table nicely
library(knitr)


kable(presdf[,1:3]) # other two columns are unneeded
```

It seems like the more compact methods take quite longer than the ones that pre-allocate space and refrain from matrix operations.


c.  Profile the code to explain why the two new versions disappoint relative to the original two.  Cite particular subroutines which cause the slowdowns with reference to the profile summaries.  Are these subroutines they creating memory or computational bottlenecks, or both?

```{r profiling section}
## Start profiler for the below section of code
Rprof("powers1.Rprof")
p1 <- powers1(x, 16)
Rprof(NULL) # To stop
p1.Rprof <- summaryRprof("powers1.Rprof")

Rprof("powers2.Rprof")
p2 <- powers2(x, 16)
Rprof(NULL) # To stop
p2.Rprof <- summaryRprof("powers2.Rprof")

Rprof("powers3.Rprof")
p3 <- powers3(x, 16)
Rprof(NULL) # To stop
p3.Rprof <- summaryRprof("powers3.Rprof")

Rprof("powers4.Rprof")
p4 <- powers4(x, 16)
Rprof(NULL) # To stop
p4.Rprof <- summaryRprof("powers4.Rprof")

## For memory usage
Rprofmem("powers1.Rprofmem")
p1 <- powers1(x, 16)
Rprofmem(NULL) # To stop
p1.Rprofmem <- noquote(readLines("powers1.Rprofmem", n = 5))

Rprofmem("powers2.Rprofmem")
p2 <- powers2(x, 16)
Rprofmem(NULL) # To stop
p2.Rprofmem <- noquote(readLines("powers2.Rprofmem", n = 5))

Rprofmem("powers3.Rprofmem")
p3 <- powers3(x, 16)
Rprofmem(NULL) # To stop
p3.Rprofmem <- noquote(readLines("powers3.Rprofmem", n = 5))

Rprofmem("powers4.Rprofmem")
p4 <- powers4(x, 16)
Rprofmem(NULL) # To stop
p4.Rprofmem <- noquote(readLines("powers4.Rprofmem", n = 5))



kable(p1.Rprof$by.self[1:3,],caption = "Powers 1 time table")
kable(p2.Rprof$by.self[1:2,],caption = "Powers 2 time table")
kable(p3.Rprof$by.self[1,],caption = "Powers 3 time table")
kable(p4.Rprof$by.self[1:9,],caption = "Powers 4 time table")

## A table for presenting the memory information
presmemdf <- cbind(p1.Rprofmem, p2.Rprofmem, p3.Rprofmem, p4.Rprofmem)
colnames(presmemdf) <- c("Powers 1", "Powers 2", "Powers 3", "Powers 4")



## This table, commented below, does not format well, so I'm just going 
## to print the numbers and corresponding functions

#kable(presmemdf, caption = "Memory usage for all 4 powers functions")

nums <- cbind(c(80000048, 80000048),
              c(640000048, 40000048),
              c(640000048, 1280000048),
              c(1280000048, 1280000048))

descp <- cbind(c("matrix, powers1", "powers1"),
               c("matrix, powers2", "powers2"),
               c("outer, powers3", "outer, powers3"),
               c("matrix, powers4", "matrix powers4"))

presmemdf2 <- cbind(nums[,1],descp[,1],
                    nums[,2],descp[,2],
                    nums[,3],descp[,3],
                    nums[,4],descp[,4])
colnames(presmemdf2) <- c("P1 bytes", "P1 obj", "P2 bytes", "P2 obj", 
                          "P3 bytes", "P3 obj", "P4 bytes", "P4 obj")

rownames(presmemdf2) <- c("4th entry","5th entry")

kable(t(presmemdf2), caption = "Memory usage for all 4 powers functions")

```

Above are 4 tables, 1 for each powers function, that show which components in each function take the most amount of time. As seen in the notes, the cbind function in powers1 takes the majority of time, both in self and total time. In powers2, every operation has been boiled down, as the matrix allocation takes up the most time. In powers3, the most compact function, as we expect shows the outer function taking the majority of the time. This trend is likely explained by the massive matrix created by the 16 powers. Lastly, in the powers4 function, we see a similar phenomenon as the apply function takes a large chunk of time in both self and total. 

With regards to memory, it seems that both powers 3 and powers 4 have much large memory allocation than powers 1 and 2. Powers 1 uses 80 MB, powers 2 uses 640 MB in the allocation of the matrix, but only uses 40 MB in the raw function. This pales in comparison to powers 3 and 4, which use 1.28 GB in each of their calculations in RAM. Using both time spent and memory allocation, it seems like there is a memory bottleneck in both powers 3 and 4, with computational bottlenecks in both. 

## Problem 2: Annie & Sam (15 pts)

How would adjust the code for the Annie \& Sam example(s) to accommodate other distributions?  E.g., if $S \sim \mathcal{N}(10.5, 1)$ and $A\sim\mathcal{N}(11, 1.5)$?

To adjust the code, I would simply replace the uniform distribution with the normal distribution and rerun the same code after drawing Annie's and Sam's times.

Code is below:

```{r Annie Sam normal}

n <- 10000
S <- rnorm(n, 10.5, 1)
A <- rnorm(n, 11, 1.5)
prob <- mean(A < S)
prob



plot(S,A,main="Normal distribution Arrival Times")
polygon(c(5.5, 17.5, 17.5, 5.5), + c(5.5, 17.5, 17.5, 5.5), density=10, angle=135)

```


## Problem 3: Bootstrap with `boot` (15 pts)

Re-write the least-squares regression bootstrap from lecture using the `boot` library. 

- Briefly compare and contrast to the results we obtained in lecture.

```{r boot package}

## Data from lecture

n <- 200
X <- 1/cbind(rt(n, df=1), rt(n, df=1), rt(n,df=1)) 
beta <- c(1,2,3,0) 
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3) 
fit <- lm(Y ~ X)
coef(fit)
summary(fit)$cov.unscaled

## Lecture Bootstrap
B <- 10000
beta.hat.boot <- matrix(NA, nrow=B, ncol=length(beta))
for(b in 1:B) {
   indices <- sample(1:nrow(X),nrow(X), replace=TRUE)
   beta.hat.boot[b,] <- coef(lm(Y[indices]~X[indices,]))
}

## Boot library bootstrap
library(boot)

## Combined dataframe for y's and X's
combdf <- data.frame(Y,X) 

## This function takes any formula, creates a linear model, and outputs its coefs
coefs.boot <- function(f, dat, ind) {
   fit <- lm(f, data=dat[ind,])
   return(coef(fit))
}


bootobj <- boot(data=combdf,statistic = coefs.boot, R=10000, f=Y~.)
colMeans(bootobj$t)
```

```{r compare bootstrap ests}

## From lecture

xt <- seq(-2,2,length=10000) 
par(mfrow=c(2,2))
hist(beta.hat.boot[,1],freq=F,main="Lecture intercept",xlab="0")
ft0 <- dnorm(xt,mean=coef(fit)[1],sd=sqrt(summary(fit)$cov.unscaled[1,1]))
lines(xt,ft0,col="red")

xt <- seq(1,3,length=10000) 
hist(beta.hat.boot[,2],freq=F,main="Lecture beta 1",xlab="1")
ft1 <- dnorm(xt,mean=coef(fit)[2],sd=sqrt(summary(fit)$cov.unscaled[2,2]))
lines(xt,ft1,col="red")

xt <- seq(2.5,3.5,length=10000) 
hist(beta.hat.boot[,3],freq=F,main="Lecture beta 2",xlab="2")
ft2 <- dnorm(xt,mean=coef(fit)[3],sd=sqrt(summary(fit)$cov.unscaled[3,3]))
lines(xt,ft2,col="red")

xt <- seq(-0.5,0.5,length=10000) 
hist(beta.hat.boot[,4],freq=F,main="Lecture beta 3",xlab="3")
ft3 <- dnorm(xt,mean=coef(fit)[4],sd=sqrt(summary(fit)$cov.unscaled[4,4]))
lines(xt,ft3,col="red")


## Boot package bootstrap

xt <- seq(-2,2,length=10000) 
par(mfrow=c(2,2))
hist(bootobj$t[,1],freq=F,main="Boot library intercept",xlab="0")
ft0 <- dnorm(xt,mean=coef(fit)[1],sd=sqrt(summary(fit)$cov.unscaled[1,1]))
lines(xt,ft0,col="red")

xt <- seq(1,3,length=10000) 
hist(bootobj$t[,2],freq=F,main="Boot library beta 1",xlab="1")
ft1 <- dnorm(xt,mean=coef(fit)[2],sd=sqrt(summary(fit)$cov.unscaled[2,2]))
lines(xt,ft1,col="red")

xt <- seq(2.5,3.5,length=10000) 
hist(bootobj$t[,3],freq=F,main="Boot library beta 2",xlab="2")
ft2 <- dnorm(xt,mean=coef(fit)[3],sd=sqrt(summary(fit)$cov.unscaled[3,3]))
lines(xt,ft2,col="red")

xt <- seq(-0.5,0.5,length=10000) 
hist(bootobj$t[,4],freq=F,main="Boot library beta 3",xlab="3")
ft3 <- dnorm(xt,mean=coef(fit)[4],sd=sqrt(summary(fit)$cov.unscaled[4,4]))
lines(xt,ft3,col="red")


```

Looking at the histograms above, we can see that the boot package performs similarly to the homemade bootstrap from in the lecture. To use the boot function, all I needed to specify was data, replicates, and the statistic I want computed each time. I chose to compute the coefficients each time and we can see that the sampling distributions of each bootstrap statistic closely resemble one another.

## Problem 4: Spam MC shell script (15 pts)

Design a shell script `spam_mc.sh` which automates a crop of batch Monte Carlo instances for our spam "bakeoff", which are to be run in parallel.  It must take an integer augment specifying how many parallel instances to create.  For example, make it so one can execute

```
./spam_mc.sh 16
```

to get 16 batches in parallel.  Some warnings and specifications:

- You will need to do some research (i.e., Googling) to get help with Bash scripting here.
- The code must work appropriately for any (positive integer) argument provided, with a sensible default when no argument is provided.  Additionally, it must provide a warning when the argument is greater than the number of cores on the machine.  For some help on that (and as an example of Googling for help), see: [https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line](https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line).  
- If you choose to work in OSX on your Mac, make sure to test your implementation on your Ubuntu virtual machine.
 
Be careful to ensure that any temporary files used by each instance do not trample on others.

```{r readin data and boxplot}
df <- read.csv("spamhitmiss.csv")

## Draw a box plot for each method
boxplot(df[,-c(1,2)],main="Avg (across folds) percent correct \n classification for each method",
        xlab="Method", ylab="% correct")
```


Answer: I have ran 6 parallel instances each for 6 replications, totalling for 36 total replications for this boxplot. As we see above, random forests perform the best, around 95% correct classification of the spam email. 


## Problem 5: Spam MC "bakeoff" in `parallel` (35 pts)

Re-write the spam MC "bakeoff" from lecture with sockets (i.e., via the `parallel` package) rather than via "batch mode".  

- One thing this means is that you need to collate the list of outputs `clusterApply` provides; i.e., not by reading `*.Rdata` files.

You must also provide (via Bitbucket):

- An R script called `spam_snow.R` that runs the entire "bakeoff" using four parallel instances (sockets), by default.
- A shell script called `spam_snow.sh` that takes an integer command-line argument specifying the number of parallel instances (sockets) to create, the number of CV folds, and the number of CV repetitions.  Alternatively, you can make `spam_snow.R` directly executable (with the same command-line argument).  Please indicate which in your solution and/or in a `README.md` file on Bitbucket.  Whatever you choose, make sure to have a warning if the argument provided implies more instances than cores, as in Problem 5.

