# Title: README.md

## Author: Shane Bookhultz

This folder contains all necessary files to run all the problems in homework 3. Types of files and purposes are explained below.

## Executables:
### 1. spam_mc.sh: An executable file run on the given spam_mc.R file to produce parallel batch instances. Syntax: spam_mc.sh 16 produces 16 parallel instances (1 if no argument is provided), each with 5 cross validation folds and 6 repetitions for each instance.  
### 2. spam_snow.sh: An executable file run on spam_snow.R. This produces parallelizability through only the R file. The shell script takes three inputs: number of cores, number of cross validation folds, and the number of repetitions total. Default values are cores=4, folds=5, reps=30. Syntax: spam_snow.sh 3 4 5 runs the spam_snow.R file with 3 cores, 4 cross validation folds in each of the 5 total repetitions.

## R files:
### 1. hw3.Rmd: Main R file to be compiled. Runs questions 1-3 and shows a boxplot based on question 4. Questions 4 and 5 are run separately, they take too much time to compile. 
### 2. spam_mc.R: Original file from class that runs all the models for solving correct classification of spam emails. Will be called from spam_mc.sh
### 3. spam_snow.R: File for question 5 that runs the models from spam_mc.R, but this file is self contained and runs in parallel via the parallel package. Output from this is saved in Listdata.RData and Matdata.RData.
### 4. spam_mc_collect.R: Same file in lecture used to aggregate all RData files generated from spam_mc.sh into one combined file. I have made one addition, that the summarized information over folds is saved to spamhitmiss.csv.

## Data:
### 1. spam.csv: Original spam data with X's and y's to be used in both spam_mc.R and spam_snow.R.
### 2. spamhitmiss.csv: Output information from spam_mc_collect.R, which has summarized information for all methods used in Questions 4 and 5. This data is used to produce a boxplot in hw3.Rmd.  
