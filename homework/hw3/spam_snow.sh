#!/bin/bash

# Take in N above
# Input order is 
# Sockets
# CV fold number
# CV rep number

N=$1
cores=`grep -c ^processor /proc/cpuinfo`

if [ $# -eq 0 ]
then
	echo "No argument, will use only 4 threads"
	N=4
	CVfold=5
	rep=30
else
	N=$1
	CVfold=$2
	rep=$3
fi

echo "$N $CVfold $rep"

if [ $N -gt $cores ]
then 
	echo "This machine has $cores threads, please choose a smaller number"
	exit 1
fi

nohup Rscript spam_snow.R cores=$N folds=$CVfold reps=$rep &

