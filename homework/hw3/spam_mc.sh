#!/bin/bash

# Take in N above

N=$1
cores=`grep -c ^processor /proc/cpuinfo`

if [ $# -eq 0 ]
then
	echo "No argument, will use only 1 thread"
	N=1
fi

if [ $N -gt $cores ]
then 
	echo "This machine has $cores threads, please choose a different number"
	exit 1
fi

# if [ $2 -eq 0 ]
# then
# 	rep=30
# else
# 	rep=$2
# fi


for (( iter=1; iter<=$N; iter++ ))
do
	nohup R CMD BATCH "--args seed=$iter reps=6" spam_mc.R spam_mc_$iter.Rout &
done
