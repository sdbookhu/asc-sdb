# Title: README.md

## Author: Shane Bookhultz

This file contains details explaining the "hw2" directory.

### Files:
#### 1. hw2.Rmd is the original hw2 uploaded by Dr. Gramacy, with no solutions
#### 2.  hw2_sol.Rmd is an rmarkdown document containing the write-up and solutions for hw2. 
#### 3.  hw2_sol_compile.R is an Rscript with 2 lines, 1 to render the Rmd doc, and 1 to convert all the code into a .R file to easily run using Rscript/R CMD. 
#### 4.  build.sh is a script file that runs hw2_sol_compile.R through bash. 
#### 5.  times_mac.RData and times_mac_swapdo.RData are data files containing times to the swap functions in 33b and 3c in the homework. 

### Generated Documents:
#### 1.  hw2_sol.html is the compiled Rmd document, hw2_sol.Rmd, into html
#### 2.  hw2_sol.R is all of the R code corresponding to the hw2_sol.Rmd document. 

### Overall:

To compile the rmarkdown html file and corresponding R file, just run ./build.sh which will run the unix script that calls the Rscript file that makes the html and R file. 
