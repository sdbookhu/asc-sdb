---
title: "Homework 2"
subtitle: "Advanced Statistical Computing (STAT 6984)"
author: "Shane Bookhultz (<sdbookhu@vt.edu>) <br> Department of Statistics, Virginia Tech"
output: html_document
---


## Instructions

This homework is **due on Wednesday, February 26th at 1:25pm** (the start of class). Please turn in all your work. The purpose of this homework is to explore some of the details of R and update Unix skills.  This description may change at any time, however notices about substantial changes (requiring more/less work) will be additionally noted on the class web page.  Note that there are two prongs to submission, via Canvas and Bitbucket (in `asc-repo/hwk/hw2`).  You don't need to use `Rmarkdown` but your work should be just as pretty if you want full marks.

### Problem 1: Unix commands (15 pts)

Update your `asc-repo/notes/unix.txt` file to include the commands we have discussed in class since homework 1, and any others that you'd like to keep a list of.  (Note that the answer to this question does not require files in `asc-repo/hw2`.)

#### Answer:

Completed, added commands from the past 2 weeks in the unix.txt file. 

### Problem 2: Immutable objects (30 pts)

Consider the following function which swaps elements `i` and `j` of a vector `v`.

```{r}
swap <- function(v, i, j) 
  { 
	tmp <- v[i]
	v[i] <- v[j]
	v[j] <- tmp 
}
``` 

A disadvantage of this implementation is that it copies the entire vector, `v`, in order to work with just two of its elements.  Consider the following example.

```{r}
v <- 1:1000000000
system.time(swap(v, i=1, j=2))
``` 

- Nearly a second to "touch" three numbers isn't super speedy.

Your tasks on this problem are the following.

a.  Report on how much time it takes to swap two elements (`i=1; j=2`) directly on the command line, i.e., without wrapping in a function.

```{r} 
system.time({
  tmp <- v[1]
  v[1] <- v[2]
  v[2] <- tmp
})
```

Looks like it takes around 1 second without the function, in the command line. Depending on other evaluations of the computer/processor this can take up to 8-10 seconds. 

b.  Write a new version of the `swap` function, called `swap.eval`, which uses `quote` and `eval` to perform the calculation just like in part a. but within the function environment and without copying `v` by working on `v` in the `parent.frame`.  Although this is a toy example, a similar code might be useful if, say, indicies `i` and `j` required substantial pre-calculation within the function before the swap occurred.  Demonstrate your `swap.eval` with `i=1; j=2` and report on the time.

```{r}
v <- 1:1000000000
i <- 1; j <- 2
swap.eval <- function(){eval(quote({tmp <- v[i]; v[i] <- v[j]; v[j] <- tmp}),envir=.GlobalEnv)}

times1 <- matrix(NA,nrow=2,ncol=20)

for(x in 1:20) {
  times1[1,x] <- system.time(swap(v,i,j))[3]
  times1[2,x] <- system.time(swap.eval())[3]
} 
```


```{r}
par(mfrow=c(1,2))
boxplot(times1[1,],main="Swap",ylim=c(0,max(times1)))
boxplot(times1[2,],main="Swap.eval",ylim=c(0,max(times1)))

```

While both of these functions act similarly in Rstudio, their times differ (run 20 times) when ran in the terminal compared to interactively in Rmarkdown. In Rmarkdown it seems that both functions take anywhere between 1.5 and 3 seconds to compute the swap. In R terminal running on my mac, I ran the same functions for 20 times and achieved the times below. 

```{r}
# Loading in mac R terminal on swap.eval function
load("times_mac.RData")
par(mfrow=c(1,2))
boxplot(times[1,],main="Swap times on mac prompt",ylim=c(0,2.5))
boxplot(times[2,],main="Swap.eval times on mac prompt",ylim=c(0,2.5))
```

As we can see above, with running these functions in the R command prompt/console, that the first time swap.eval runs it takes the same amount of time as the swap, but after the first iteration the swap evaluation takes no time at all. This is due to the v being modified and not copied in the function environment in swap.eval compared to swap, which does copy the "v" vector at every evaluation of the function. 

c.  Write a similar function named `swap.do` which can be called via `do.call` that similarly accesses `v` in the parent frame.  Add a `print` statement at the end of `swap.do` to show the first five elements of `v` after the swap occurs.  Demonstrate `swap.do` with `i=1; j=2` and report on the time.  Are there any disadvantages to `swap.do` compared to `swap.eval`?

```{r}
# Parent frame is global env
v <- 1:100000000
i <- 1
j <- 2

# Function to run the swap.do on
f <- function(i,j) {
  tmp <- v[i]; 
  v[i] <- v[j]; 
  v[j] <- tmp
  print(v[1:5])
}

swap.do <- function(i,j) {
  do.call("f",list(i,j),envir = .GlobalEnv)
}

```

Down below I will show the swap.do evaluated on my windows subsystem for linux, and the exact same function run in mac R terminal.

```{r}

times.windowssub <- rep(NA,times=20)
for(k in 1:20){
	times.windowssub[k] <- system.time({swap.do(1,2)})[3]
}

# Load in time data, called times.docall
load("times_mac_swapdo.RData")
par(mfrow=c(1,2))
boxplot(times.windowssub,main="Swap.do on windows linux",ylim=c(0,max(times.docall,times.windowssub)))
boxplot(times.docall,main="Swap.do on mac R terminal", ylim = c(0,max(times.docall,times.windowssub)))

```

As we can see from above, the swap.do runs quite faster than the original swap function. The main disadvantage of swap.do compared to swap.eval is that after the first call of swap.eval on the same vector, that the vector is not copied any longer. Therefore, for longer vectors swap.eval runs much faster than swap.do, this is the main disadvantage. After running this code for 20 times, we can see that the behavior does not significantly in this example between tries. 


*Note: be sure to try your codes/functions several times to see if the behavior changes from one try to the next.*


### Problem 3: Bisection broadening (30 pts)
 
Recall the bisection algorithm and `S3` object-oriented implementation from class.  The bisection method can be generalized to deal with the case $f(x_l) f(x_r) > 0$, by *broadening* the bracket.  That is,  

- reduce $x_l$ and/or increase $x_r$, and try again.
- A reasonable choice is to double the width of the interval, i.e., 

$$
\begin{aligned}
m &\leftarrow (x_l + x_r)/2, & w &\leftarrow x_r - x_l \\
x_l&\leftarrow m - w, & x_r &\leftarrow m + w.
\end{aligned}
$$

Your tasks are the following.

a.  Incorporate bracketing into the \R{bisection()} function we coded. Note that broadening is *not guaranteed* to find $x_l$ and $x_r$
such that $f(x_l) f(x_r) \leq 0$, so you should include a limit on the number of times broadening is successively tried with a sensible default.

```{r 3a}

## bisection method function; must have xl <- xr
## and f(xl)*f(xr) < 0;  output is a root of f
## in the interval (xl, xr)
bisection <- function(f, xl, xr, tol=sqrt(.Machine$double.eps),
                      verb=0)
  {
    ## create an output object
    out <- list(f=f, tol=tol)
    class(out) <- "bisection"
    
    ## check inputs
    if(xl > xr) stop("must have xl < xr")

    ## setup and check outputs
    fl <- f(xl)
    fr <- f(xr)
    out$numbro <- 0
    lim <- 1
    out$prog <- data.frame(xl=xl, xr=xr, fl=fl, fr=fr)

    if(fl == 0) {
      out$ans <- xl; return(out) 
    }
    else if(fr == 0) { 
      out$ans <- xr; return(out) 
    }
    else if(fl * fr > 0) {
      # Broadening the bracket, need to put a limit
      # Put a limit of 10 times 
      maxlim <- 10
      while(lim <= maxlim && fl * fr >= 0) {
        # Broaden
        m <- (xl + xr)/2
        w <- xr - xl
        xl <- m - w
        xr <- m + w
        
        # Apply f to new end points
        fl <- f(xl)
        fr <- f(xr)
              
        # Update counters for lim and broadening
       
        lim <- lim + 1
        out$numbro <- out$numbro + 1
        out$prog[lim,] <- c(xl, xr, fl, fr)
      }
    
      ## If we have exceeded our limit evaluations
      ## And still not found something, then break
      if(lim == (maxlim+1) && fl * fr < 0) {
        stop("f(xl) * f(xr) > 0")
      }
    }
    ## successively refine xl and xr
    n <- lim
    while((xr - xl) > tol) {
      xm <- (xl + xr)/2
      fm <- f(xm)
      if(fm == 0) { out$ans <- xm; return(out) }
      else if (fl * fm < 0) {
        xr <- xm; fr <- fm
      } else { xl <- xm; fl <- fm }

      ## next iteration
      n <- n + 1
      out$prog[n,] <- c(xl, xr, fl, fr)
      if(verb > 0)
        cat("n=", n, ", (xl, xr)=(", xl, ", ", xr, ")\n", sep="")
    }

    ## return (approximate) root
    out$ans <- (xl + xr)/2
    return(out)
  }

## ------------------------------------------
# For simplicity, I will put the associated #
# S3 functions with bisection below         #
## ------------------------------------------


# Modified slightly print S3
print.bisection <- function(x, ...) {
  cat("Root of:\n")
  print(body(x$f)) 
  cat("starting in (", x$prog$xl[1], ", ", x$prog$xr[1],                                         ") found after ", nrow(x$prog), " iterations: ",                                   x$ans, "\n", "to a tolerance of ", x$tol, "\n",
      "with ", x$numbro, " broadening steps\n",sep="")
}


# Unchanged summary function, seems good as-is
summary.bisection <- function(object, ...) {
  print(object, ...)
  cat("\nProgress is as follows\n")
  print(object$prog)
}

plot.bisection <- function(object, gridlen=1000, after=0, ...) {
  
  # Sanity check for input
  if(nrow(object$prog) == 0) {
    cat("Check inputs, nothing to plot\n")
    invisible(NULL)
  }
  
  xgrid <- seq(object$prog$xl[1], object$prog$xr[1], length=gridlen)
  xgrid <- sort(union(xgrid, c(object$prog$xl, object$prog$xr)))
  y <- object$f(xgrid) ## assumes a vectorized function
  n <- nrow(object$prog)
  if(after > 0) {
    ## Change plot to be zoomed in
    ## Need to change boundaries for plotting
    xl.sub <- object$prog$xl[(after:n)] 
    xr.sub <- object$prog$xr[(after:n)]
    yl.sub <- object$prog$fl[(after:n)]
    yr.sub <- object$prog$fr[(after:n)]
    
    ## Adjust the vectors for dupes and correct limits
    dl <- duplicated(xl.sub)
    dr <- duplicated(xr.sub)
    dl <- !dl; dr <- !dr
    limits <- c(min(xl.sub),max(xr.sub))
    limits.y <- c(min(c(yl.sub,yr.sub)),max(c(yl.sub,yr.sub)))
    
    ## if statements for checking limits 
    ## Stop user if no data to plot
    if(sum(dl) == 0) {
      warning("Only upper bound is changing",sep="")
      limits[1] <- max(object$prog$xl)
    }
    if(sum(dr) == 0) {
      warning("Only lower bound is changing",sep="")
      limits[2] <- min(object$prog$xr)
    }
    if(after > n) {
      stop("\n You have nothing to plot, n = ",n,sep="")
    }
    
    ## Plot the zoomed in data
    plot(xgrid, y, type="l", col="gray",
         xlim = limits, ylim=limits.y, main=paste0("Root finding with zoom, points: ",after,"-",n), ...)
    text(xl.sub[dl], object$prog$fl[after:n][dl], ((after):n)[dl])
    text(xr.sub[dr], object$prog$fr[after:n][dr], ((after):n)[dr], col=2)
  }
  else {
    ## Regular plotting without zooming in
    dl <- duplicated(object$prog$xl)
    dr <- duplicated(object$prog$xr)
    
    # Just indexing by non-dupes
    dl <- !dl; dr <- !dr
    plot(xgrid, y, type="l", col="grey", main="Root finding without zoom" ,...)
    text(object$prog$xl[dl], object$prog$fl[dl], (1:n)[dl])
    text(object$prog$xr[dr], object$prog$fr[dr], (1:n)[dr], col=2)
    
  }

  ## adds a reference line
  abline(h=0, col=3, lty=3)
} 

  

```



b.  Use your modified function to find a root of the (original) function $f(x)$ we used in class, but with a different starting interval of $x_l = 2$ and $x_r = 3]$, i.e., not containing the root we found in class.

```{r testing out bisection}

ff <- function(x) {
  log(x)-exp(-x)
}

bb <- bisection(ff, xl=2, xr=3, tol=sqrt(.Machine$double.eps),verb=1)

print(bb)
summary(bb)
plot(bb)
# Show the plot zoomed in after 10 iterations
plot(bb,after=10)
```

We can see the root around 1.3098, and after 10 iterations the bisection algorithm is narrowing in on the root.  

c.  Use your modified function find the root of
$$
h(x) = (x - 1)^3 - 2x^2 - \sin(x),
$$
starting with $x_l = 1$ and $x_r = 2$.


```{r 3c}
# Finding root with new function
f3c <- function(x) {(x-1)^3 - 2*(x^2) - sin(x)}

rootfind <- bisection(f3c, xl=1, xr=2,verb=1)
print(rootfind)
summary(rootfind)
plot(rootfind,gridlen=50000)
plot(rootfind,after=10)
```

We can see above that the bisection needs to expand 3 times to have the interval contain a root of the function. After this expansion, the algorithm narrows in, comes within tolerance of the true root. At 10 iterations and beyond, the interval is narrowing down to the root. I have also coded user input errors, such as inputting an after number which is greater than the number of intervals we have to plot. Additionally, some warnings are provided in the "after" section that note that one side of the interval is not improving.  

For full credit you must keep everything in the S3 environment with appropriate modifications to your generic methods, etc.  You will be judged on style here, in terms of code, `S3` behavior, and writeup/demonstration.  You may have a separate `bisection.R` file with your `S3` library functions, however your writeup must verbally describe how those functions have changed.  I will check for `bisection.R` in your repository against your description.


### Problem 4: R scripts from the Unix prompt (25 pts)

R provides two "commands" to execute scripts (e.g., some R code in a file like `script.R`).  What are those commands and what are their differences?  Please provide a *brief* high level description and do not plagiarize.  Focus on details like:

- What happens with plots?
- What happens with text output that would normally be printed to the screen?  What about warnings or errors?  
- What happens with the objects in the workspace when the script terminates.

And be sure to contrast default behavior with options for customization.

#### Answer:

The two "commands" to execute scripts are Rscript and R CMD BATCH. Running Rscript with a regular R file puts plots in a file named "Rplots.pdf" which contains all plots created from that file into the one pdf. R CMD BATCH still puts the plots in the "Rplots.pdf" file, but also creates a file, file extention .Rout, with all the code ran and the time it took to run that code. Rscript shows the text output in the terminal that would normally be printed to the screen, but R CMD BATCH puts that text information in the .Rout file following the statement that prints the text. Rscript by default prints out the error to the screen, while similarly to printing text, R CMD BATCH shows the error in the .Rout file following the erroneous statement. Rscript will not save objects in the workspace when the script terminates by default, but if one specifies "--save" then the objects will be saved in the workplace. R CMD BATCH by default will save the objects in the workspace when the script terminates. So if I would open up an R session after my script terminates, I can access my objects run from my R file by default from R CMD BATCH and by --save from Rscript. 

The above paragraph explains the default behavior from both commands, but there are multiple customization options in both commands. For example, Rscript can evaluate expressions via the "-e" argument which can allow for easy rendering of Rmd doucments. Additionally, Rscript has the ability to print out verbose information related to R and the file being run. R CMD has a few more options than Rscript, that being one can run in batches, can manipulate add-on packages in the command statement, and also convert Rd documents to different formats.


Write an R script which renders an `Rmarkdown` document in HTML and furnishes a companion `.R` file containing an extraction of the raw code.  The script must entirely comprise of `R` commands (no manual steps like clicking buttons in RStudio).  If you are composing this solution in `Rmarkdown` you may use your homework file `hw2_sol.Rmd` as an example file, generating `hw2_sol.html` and `hw2_sol.R`.  Otherwise, you will need to create a simple dummy one for the purposes of illustration.  Then describe the *single* Unix command that you would need to call from the command prompt to cause your `.html` and `.R` file to be generated from the `Rmd` file.

Create a shell script, which is a text file, called `build.sh` with the following two lines

```
#!/bin/bash
Rscript hw2_sol_compile.R
```

Make the script file executable with the command `chmod +x build.sh`, and now you can run it from the command line with `./build.sh`.  Make sure it all works, and that it has been added to your repository in `hw2/` because it will be tested.

(Note, don't add the output `.html` and `.R` files to your repository.  Don't forget to add the new commands you've learned to your `unix.txt` file.)
