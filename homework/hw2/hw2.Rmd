---
title: "Homework 2"
subtitle: "Advanced Statistical Computing (STAT 6984)"
author: "Robert B. Gramacy (<rbg@vt.edu> : <http://bobby.gramacy.com>) <br> Department of Statistics, Virginia Tech"
output: html_document
---


## Instructions

This homework is **due on Wednesday, February 19th at 1:25pm** (the start of class). Please turn in all your work. The purpose of this homework is to explore some of the details of R and update Unix skills.  This description may change at any time, however notices about substantial changes (requiring more/less work) will be additionally noted on the class web page.  Note that there are two prongs to submission, via Canvas and Bitbucket (in `asc-repo/hwk/hw2`).  You don't need to use `Rmarkdown` but your work should be just as pretty if you want full marks.

### Problem 1: Unix commands (15 pts)

Update your `asc-repo/notes/unix.txt` file to include the commands we have discussed in class since homework 1, and any others that you'd like to keep a list of.  (Note that the answer to this question does not require files in `asc-repo/hw2`.)

### Problem 2: Immutable objects (30 pts)

Consider the following function which swaps elements `i` and `j` of a vector `v`.

```{r}
swap <- function(v, i, j) 
  { 
	tmp <- v[i]
	v[i] <- v[j]
	v[j] <- tmp 
  }
``` 

A disadvantage of this implementation is that it copies the entire vector, `v`, in order to work with just two of its elements.  Consider the following example.

```{r}
v <- 1:100000000#0
system.time(swap(v, i=1, j=2))
``` 

- Nearly a second to "touch" three numbers isn't super speedy.

Your tasks on this problem are the following.

a.  Report on how much time it takes to swap two elements (`i=1; j=2`) directly on the command line, i.e., without wrapping in a function.
b.  Write a new version of the `swap` function, called `swap.eval`, which uses `quote` and `eval` to perform the calculation just like in part a. but within the function environment and without copying `v` by working on `v` in the `parent.frame`.  Although this is a toy example, a similar code might be useful if, say, indicies `i` and `j` required substantial pre-calculation within the function before the swap occurred.  Demonstrate your `swap.eval` with `i=1; j=2` and report on the time.
c.  Write a similar function named `swap.do` which can be called via `do.call` that similarly accesses `v` in the parent frame.  Add a `print` statement at the end of `swap.do` to show the first five elements of `v` after the swap occurs.  Demonstrate `swap.do` with `i=1; j=2` and report on the time.  Are there any disadvantages to `swap.do` compared to `swap.eval`?

*Note: be sure to try your codes/functions several times to see if the behavior changes from one try to the next.*


### Problem 3: Bisection broadening (30 pts)
 
Recall the bisection algorithm and `S3` object-oriented implementation from class.  The bisection method can be generalized to deal with the case $f(x_l) f(x_r) > 0$, by *broadening* the bracket.  That is,  

- reduce $x_l$ and/or increase $x_r$, and try again.
- A reasonable choice is to double the width of the interval, i.e., 

$$
\begin{aligned}
m &\leftarrow (x_l + x_r)/2, & w &\leftarrow x_r - x_l \\
x_l&\leftarrow m - w, & x_r &\leftarrow m + w.
\end{aligned}
$$

Your tasks are the following.

a.  Incorporate bracketing into the \R{bisection()} function we coded. Note that broadening is *not guaranteed* to find $x_l$ and $x_r$
such that $f(x_l) f(x_r) \leq 0$, so you should include a limit on the number of times broadening is successively tried with a sensible default.

b.  Use your modified function to find a root of the (original) function $f(x)$ we used in class, but with a different starting interval of $x_l = 2$ and $x_r = 3]$, i.e., not containing the root we found in class.

c.  Use your modified function find the root of
$$
h(x) = (x - 1)^3 - 2x^2 - \sin(x),
$$
starting with $x_l = 1$ and $x_r = 2$.

For full credit you must keep everything in the S3 environment with appropriate modifications to your generic methods, etc.  You will be judged on style here, in terms of code, `S3` behavior, and writeup/demonstration.  You may have a separate `bisection.R` file with your `S3` library functions, however your writeup must verbally describe how those functions have changed.  I will check for `bisection.R` in your repository against your description.


### Problem 4: R scripts from the Unix prompt (25 pts)

R provides two "commands" to execute scripts (e.g., some R code in a file like `script.R`).  What are those commands and what are their differences?  Please provide a *brief* high level description and do not plagiarize.  Focus on details like:

- What happens with plots?
- What happens with text output that would normally be printed to the screen?  What about warnings or errors?  
- What happens with the objects in the workspace when the script terminates.

And be sure to contrast default behavior with options for customization.

Write an R script which renders an `Rmarkdown` document in HTML and furnishes a companion `.R` file containing an extraction of the raw code.  The script must entirely comprise of `R` commands (no manual steps like clicking buttons in RStudio).  If you are composing this solution in `Rmarkdown` you may use your homework file `hw2_sol.Rmd` as an example file, generating `hw2_sol.html` and `hw2_sol.R`.  Otherwise, you will need to create a simple dummy one for the purposes of illustration.  Then describe the *single* Unix command that you would need to call from the command prompt to cause your `.html` and `.R` file to be generated from the `Rmd` file.

Create a shell script, which is a text file, called `build.sh` with the following two lines

```
#!/bin/bash
insert your unix command for building from hw2_sol.Rmd here
```

Make the script file executable with the command `chmod +x build.sh`, and now you can run it from the command line with `./build.sh`.  Make sure it all works, and that it has been added to your repository in `hw2/` because it will be tested.

(Note, don't add the output `.html` and `.R` files to your repository.  Don't forget to add the new commands you've learned to your `unix.txt` file.)
